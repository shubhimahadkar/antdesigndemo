import React from 'react';
import './App.css';
import { Layout } from 'antd';
import UserList from './component/UserList';
import User from './component/User';
const { Header } = Layout;

class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      show: true,
      userInfo: []
    }
  }

  userData = (data) => {
    console.log(data)
    this.setState(
      {
        userInfo: [data],
        show: !this.state.show
      }
    )
  }

  backUserList = () => {
    this.setState({ show: !this.state.show })
  }


  render() {
    return (
      <div className="App">
        <Layout>

          <Header className='header' >
            <h1>Header</h1>
          </Header>
          <Layout>
            {
              this.state.show ?
                <UserList userData={this.userData} /> :
                <User
                  userInfo={this.state.userInfo}
                  backUserList={this.backUserList} />

            }

          </Layout>

        </Layout>

      </div>
    );
  }

}

export default App;
