import React, { Component } from 'react';
import axios from 'axios';

export class getData extends Component {
    constructor(props) {
        super(props)

        this.state = {
            posts: [],
            errorMessage: ''
        }
    }

    componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(
                (response) => {
                    console.log(response);
                    this.setState(
                        {
                            posts: response.data
                        }
                    )
                }
            )
            .catch(
                (error) => {
                    console.log(error);
                    this.setState(
                        {
                            errorMessage: 'Error retriving data'
                        }
                    )
                }
            )
    }

    render() {
        const { posts, errorMessage } = this.state
        return (
            <div>
                <p>List of post</p>
                <ul>
                    {
                        posts.length ?
                            posts.map(
                                (post) =>
                                    <li key={post.id}>{post.title}</li>
                            ) :
                            null
                    }
                    {
                        errorMessage ? <div>{errorMessage}</div> : null
                    }
                </ul>

            </div>
        )
    }
}

export default getData
