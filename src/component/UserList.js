import React, { Component } from 'react'
import { Table, Button } from 'antd'
import axios from 'axios'


const { Column } = Table;

export class UserList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            posts: [],
            errorMessage: ''
        }
    }

    componentDidMount() {
        this.getData()

    }

    getData = () => {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(
                (response) => {
                    console.log(response);
                    this.setState(
                        {
                            posts: response.data
                        }
                    )
                }
            )
            .catch(
                (error) => {
                    console.log(error);
                    this.setState(
                        {
                            errorMessage: 'Error retriving data'
                        }
                    )
                }
            )
    }

    recordUser(record) {
        console.log(record.userId)
        this.props.userData(record)
    };

    render() {

        return (
            <div>
                <Table dataSource={this.state.posts}
                    style={{
                        marginLeft: '200px',
                        marginRight: '200px',
                        marginTop: '50px',
                        marginBottom: '30px'
                    }}
                    bordered='true'
                    scroll={{ y: 300 }}
                >

                    <Column title="User Id" dataIndex="id" key="id" />
                    <Column title="Title" dataIndex="title" key="title" width='70%' />
                    <Column title="Action" key="id" align='center'
                        render={(text, record) => (
                            <Button type="link" onClick={() => this.recordUser(record)}>
                                show
                            </Button>)
                        }
                    />
                </Table>
            </div>
        )
    }
}

export default UserList
