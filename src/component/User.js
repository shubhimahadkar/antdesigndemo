import React, { Component } from 'react';
import { Layout, Card, Button } from 'antd';
const { Content } = Layout;

export class User extends Component {
    constructor(props) {
        super(props)

    }

    backButton = () => {
        this.props.backUserList()
    }


    render() {
        const { userInfo } = this.props
        return (
            <div>

                <Layout>
                    <Content
                        className="site-layout-background"
                        style={{
                            backgroundColor: 'white',
                            margin: '50px 20px',
                            padding: 30,
                            minHeight: 280,
                        }} >

                        <Button type="primary" onClick={this.backButton}>Back</Button>

                        {
                            userInfo.map(
                                (index) =>

                                    <Card title={`UserId: ${index.id}`}
                                        key={index.key}
                                    >
                                        <h3>Id is {index.id}</h3>
                                        <h4>Title : {index.title}</h4>
                                        <p>Body: {index.body} </p>

                                    </Card>

                            )
                        }
                    </Content>
                </Layout>

            </div>
        )
    }
}


export default User
