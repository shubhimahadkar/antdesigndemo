import React, { Component } from 'react';
import ShowData from './showData';


export class showSingleData extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            posts: []
        }
    }

    handleParentData=(data)=>{
        console.log(data)
        this.setState({posts:[data]})
    }

    render() {
        const { posts } = this.state
        return (
            <div>
                <ShowData handleData={this.handleParentData}/>
                <p>gcgc</p>
                {
                    posts.length ?
                    posts.map(
                        (post) =>
                            <li key={post.id}>{post.title}</li>
                    ) :
                    null
                }
            </div>
        )
    }
}

export default showSingleData
